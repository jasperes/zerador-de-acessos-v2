# -*- coding: utf-8 -*-

"""
Tela sobre o sistema.
Nada além de código para exibir esta janela.
"""

from Interface import Sobre
from PySide.QtGui import QWidget

class Sobre(QWidget, Sobre.Ui_Form):

    def __init__(self,parent=None,versao=""):
        super(Sobre,self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle("Sobre... versão %s" % versao)