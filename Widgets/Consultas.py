# -*- coding: utf-8 -*-

"""
Tela de consultas dos bancos de dados zerados.
"""

from PySide.QtGui import QWidget


from Interface import Consultas as uiConsultas
from Functions import Consultas as fnConsultas

class Consulta(QWidget, uiConsultas.Ui_Form):

    def __init__(self,parent=None,cursor=None):
        super(Consulta,self).__init__(parent)
        self.setupUi(self)
        self.connectActions()
        
        self.cur = cursor

        fnConsultas.inseri(self.cur,self.comboBox_cliente,self.comboBox_revenda,self.comboBox_dado)
        
    def connectActions(self):
        self.pushButton_reseta.clicked.connect(lambda: fnConsultas.reseta(self.comboBox_cliente,self.comboBox_revenda,self.comboBox_dado,self.comboBox_sistema,self.tableWidget))
        self.pushButton_pesquisa.clicked.connect(lambda: fnConsultas.pesquisa(self.cur,self.comboBox_cliente.currentText(),self.comboBox_revenda.currentText(),self.comboBox_dado.currentText(),self.comboBox_sistema.currentText(),self.tableWidget))
        
