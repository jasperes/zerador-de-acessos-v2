# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Consultas.ui'
#
# Created: Mon Apr 14 14:53:19 2014
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(1061, 600)
        Form.setMinimumSize(QtCore.QSize(0, 600))
        Form.setMaximumSize(QtCore.QSize(16777215, 600))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../include/img/zera.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Form.setWindowIcon(icon)
        self.gridLayoutWidget = QtGui.QWidget(Form)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 821, 54))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.comboBox_cliente = QtGui.QComboBox(self.gridLayoutWidget)
        self.comboBox_cliente.setEditable(True)
        self.comboBox_cliente.setObjectName("comboBox_cliente")
        self.gridLayout.addWidget(self.comboBox_cliente, 1, 0, 1, 1)
        self.comboBox_revenda = QtGui.QComboBox(self.gridLayoutWidget)
        self.comboBox_revenda.setEditable(True)
        self.comboBox_revenda.setObjectName("comboBox_revenda")
        self.gridLayout.addWidget(self.comboBox_revenda, 1, 1, 1, 1)
        self.label = QtGui.QLabel(self.gridLayoutWidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.label_4 = QtGui.QLabel(self.gridLayoutWidget)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 0, 1, 1, 1)
        self.comboBox_dado = QtGui.QComboBox(self.gridLayoutWidget)
        self.comboBox_dado.setEditable(True)
        self.comboBox_dado.setObjectName("comboBox_dado")
        self.gridLayout.addWidget(self.comboBox_dado, 1, 4, 1, 1)
        self.comboBox_sistema = QtGui.QComboBox(self.gridLayoutWidget)
        self.comboBox_sistema.setObjectName("comboBox_sistema")
        self.comboBox_sistema.addItem("")
        self.comboBox_sistema.addItem("")
        self.comboBox_sistema.addItem("")
        self.comboBox_sistema.addItem("")
        self.comboBox_sistema.addItem("")
        self.comboBox_sistema.addItem("")
        self.comboBox_sistema.addItem("")
        self.gridLayout.addWidget(self.comboBox_sistema, 1, 5, 1, 1)
        self.label_3 = QtGui.QLabel(self.gridLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 0, 5, 1, 1)
        self.pushButton_pesquisa = QtGui.QPushButton(self.gridLayoutWidget)
        self.pushButton_pesquisa.setObjectName("pushButton_pesquisa")
        self.gridLayout.addWidget(self.pushButton_pesquisa, 1, 6, 1, 1)
        self.pushButton_reseta = QtGui.QPushButton(self.gridLayoutWidget)
        self.pushButton_reseta.setObjectName("pushButton_reseta")
        self.gridLayout.addWidget(self.pushButton_reseta, 0, 6, 1, 1)
        self.label_2 = QtGui.QLabel(self.gridLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 4, 1, 1)
        self.tableWidget = QtGui.QTableWidget(Form)
        self.tableWidget.setGeometry(QtCore.QRect(10, 70, 1041, 521))
        self.tableWidget.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.tableWidget.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)
        Form.setTabOrder(self.comboBox_cliente, self.comboBox_revenda)
        Form.setTabOrder(self.comboBox_revenda, self.comboBox_dado)
        Form.setTabOrder(self.comboBox_dado, self.comboBox_sistema)
        Form.setTabOrder(self.comboBox_sistema, self.pushButton_pesquisa)
        Form.setTabOrder(self.pushButton_pesquisa, self.pushButton_reseta)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "Tela de Consultas", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Form", "Razão Social", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("Form", "Revenda", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_sistema.setItemText(0, QtGui.QApplication.translate("Form", "Todos", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_sistema.setItemText(1, QtGui.QApplication.translate("Form", "Ponto Secullum 4", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_sistema.setItemText(2, QtGui.QApplication.translate("Form", "Secullum Acesso.Net", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_sistema.setItemText(3, QtGui.QApplication.translate("Form", "Secullum Academia.Net", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_sistema.setItemText(4, QtGui.QApplication.translate("Form", "Secullum Clube.Net", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_sistema.setItemText(5, QtGui.QApplication.translate("Form", "Secullum Estacionamento.Net", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_sistema.setItemText(6, QtGui.QApplication.translate("Form", "Secullum Mini Folha.Net", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Form", "Sistema", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_pesquisa.setText(QtGui.QApplication.translate("Form", "Pesquisar", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_reseta.setText(QtGui.QApplication.translate("Form", "Resetar", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Form", "CNPJ/CPF", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

