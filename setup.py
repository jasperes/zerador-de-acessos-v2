# -*- coding: utf-8 -*-

from cx_Freeze import setup, Executable

includefiles = ['img/sobre.jpg',
                'img/zera.ico',
                'pymssqlce/conSQLCE.exe',
                'configs.ini',
                '7-Zip/']
includes = ['decimal','msvcrt','msilib']
excludes = []
packages = []
exe = Executable('ZeradorDeAcessos.py', 
                 base='Win32GUI', 
                 icon='img/zera.ico', 
                 shortcutName="Zerador de Acessos v2",
                 shortcutDir="DesktopFolder")
setup(
    name = 'Zerador de Acessos',
    version = '2.3',
    description = 'Zerador de acessos dos sistemas da SECULLUM',
    author = 'Jonathan Peres',
    author_email = 'jaspjonathan@gmail.com',
    options = {'build_exe': {'excludes':excludes,'packages':packages,'include_files':includefiles,'includes':includes}}, 
    executables = [exe],
    boolean_options = ["compressed", "copy_dependent_files", "include_msvcr"]
)