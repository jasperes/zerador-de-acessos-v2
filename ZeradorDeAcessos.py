# -*- coding: utf-8 -*-
# Python3

# computador principal: sswsup-0086

"""
Zerador de Acessos dos bancos de dados da Secullum.
Utilizado para zerar acessos dos bancos que excederam os 40 acessos
e gravar o cnpj/cpf, para saber quantas vezes o banco foi zerado.

ATENÇÃO: Não funciona com Python2.x
"""

__author__  = "Jonathan Peres"
__contact__ = "jaspjonathan@gmail.com"
__version__ = "2.3"

import sys

import pyodbc
import configparser

from PySide.QtGui import QApplication, QWidget
from PySide.QtGui import QFileDialog, QMessageBox

from Interface import Window
from Widgets import Consultas, Sobre
from Functions import Funcoes, Zerar

class MainWindow(QWidget,Window.Ui_MainWindow):

    def __init__(self,parent=None):
        super(MainWindow,self).__init__(parent)
        self.setupUi(self)
        self.connectActions()
        
    def main(self):
        
        self.parsers()
        self.connect()
        
        self.cur.execute("select cliente, revenda, dado, sistema from acessos_zerados")
        
        Funcoes.adicionaRevendas(self.cur, self.comboBox_revenda)
        
        self.consultas = Consultas.Consulta(cursor=self.cur)
        self.sobre = Sobre.Sobre(versao=__version__)
        
        self.show()
        
    def parsers(self):
        
        self.configs = configparser.ConfigParser()
        self.configs.read('configs.ini')
        
    def connect(self):
        
        cn = self.configs['CONNECTION']
        connection = "DRIVER={%s};SERVER=%s;DATABASE=%s;UID=%s;PWD=%s;" % (cn['driver'],cn['server'],cn['database'],cn['user'],cn['password'])
        
        try:
            self.con = pyodbc.connect(connection)
            self.cur = self.con.cursor()
            self.cur.execute("select valor from configs where nome='versao'")
            
            if float(self.cur.fetchall()[0][0]) > float(__version__):
                QMessageBox.warning(self, "Atenção!", "A versão do sistema está anterior a versão atual.\nPor favor, atualize o sistema.")
                return app.exit()
        except Exception as err:
            QMessageBox.critical(self, "Erro!", "Falha ao conectar com o banco de dados.\n\n"+str(err))
            return app.exit()
        
    def connectActions(self):
        self.comboBox_sistema.currentIndexChanged.connect(lambda: Funcoes.trocaSistema(self.comboBox_sistema.currentText(),self.comboBox_tipoBanco, self.lineEdit_nomeBanco,self.comboBox_tipoDado, self.checkBox_dadoManual.checkState()))
        self.comboBox_tipoBanco.currentIndexChanged.connect(lambda: Funcoes.trocaTipoBanco(self.comboBox_sistema.currentText(),self.comboBox_tipoBanco.currentText(),self.lineEdit_servidor,self.lineEdit_usuario,self.lineEdit_senha,self.lineEdit_nomeBanco,self.pushButton_procuraBanco))
        self.pushButton_procuraBanco.clicked.connect(lambda: Funcoes.procuraBanco(self.lineEdit_nomeBanco, QFileDialog))
        self.checkBox_dadoManual.stateChanged.connect(lambda: Funcoes.digitaDado(self.checkBox_dadoManual.checkState(),self.lineEdit_dado,self.comboBox_tipoDado,self.comboBox_sistema.currentText()))
        self.comboBox_tipoDado.currentIndexChanged.connect(lambda: Funcoes.alteraDado(self.comboBox_tipoDado.currentText(),self.lineEdit_dado))
        
        self.pushButton_sobre.clicked.connect(lambda: self.sobre.show())
        self.pushButton_zerados.clicked.connect(lambda: self.consultas.show())
        
        self.pushButton_limpar.clicked.connect(lambda: Funcoes.limpaCampos(self.lineEdit_razaoSocial,self.comboBox_tipoDado,self.lineEdit_dado,self.checkBox_dadoManual,self.comboBox_revenda,self.comboBox_sistema,self.textEdit_motivo,self.comboBox_tipoBanco,self.lineEdit_servidor,self.lineEdit_usuario,self.lineEdit_senha,self.lineEdit_nomeBanco,self.pushButton_procuraBanco))
        self.pushButton_zerar.clicked.connect(lambda: Zerar.Start(self.cur,self.con,self.configs,self.lineEdit_razaoSocial,self.comboBox_tipoDado,self.lineEdit_dado,self.checkBox_dadoManual,self.comboBox_revenda,self.comboBox_sistema,self.textEdit_motivo,self.comboBox_tipoBanco,self.lineEdit_servidor,self.lineEdit_usuario,self.lineEdit_senha,self.lineEdit_nomeBanco,self.pushButton_procuraBanco))
        
if __name__=='__main__':
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.main()
    sys.exit(app.exec_())