# -*- coding: utf-8 -*-
# Python3

"""
Ações dos widgets
"""

filter_ = "Access (*.dat);;Todos (*.*)"

def trocaSistema(sistema, tipo_banco, nome_banco, tipo_dado, status):
    
    sistema_ = (str(sistema).replace(" ","")).replace(".","")
    
    if sistema == 'Ponto Secullum 4':
        tipo_banco.clear()
        tipo_banco.addItem('Access')
        tipo_banco.addItem('SQL Server')
        tipo_banco.addItem('Oracle')
        nome_banco.clear()
        nome_banco.insert('psec.dat')
    else:
        tipo_banco.clear()
        tipo_banco.addItem('SQL Server CE')
        tipo_banco.addItem('SQL Server')
        tipo_banco.addItem('Firebird')
        tipo_banco.addItem('Oracle')
        nome_banco.clear()
        nome_banco.insert('%s.sdf' % sistema_)
    
    if sistema != "Ponto Secullum 4" or bool(status) == False:
        tipo_dado.setCurrentIndex(0)
        tipo_dado.setEnabled(0)
    else:
        tipo_dado.setCurrentIndex(0)
        tipo_dado.setEnabled(1)
        
def trocaTipoBanco(sistema, tipo_banco, servidor, usuario, senha, banco, procura_banco):
    
    global filter_
    
    a,b,c,d,e = 0,0,0,0,0
    
    sistema_ = (str(sistema).replace(" ","")).replace(".","")
    
    servidor.clear()
    usuario.clear()
    senha.clear()
    banco.clear()

    if tipo_banco == 'Access':
        a,b,c,d,e = 0,0,0,1,1
        banco.insert("psec.dat")
        filter_ = "Access (*.dat);;Todos (*.*)"
    elif tipo_banco == 'SQL Server CE':
        a,b,c,d,e = 0,0,0,1,1
        banco.insert('%s.sdf' % sistema_)
        filter_ = "SQL CE (*.sdf);;Todos (*.*)"
    elif tipo_banco == 'Firebird':
        a,b,c,d,e = 1,1,1,1,1
        servidor.insert("localhost")
        usuario.insert("sysdba")
        senha.insert("masterkey")
        banco.insert('%s.fdb' % sistema_)
        filter_ = "Firebird (*.fdb);;Todos (*.*)"
    elif tipo_banco == 'SQL Server':
        a,b,c,d,e = 1,1,1,1,0
        servidor.insert(".\sqlexpress")
        usuario.insert("sa")
        senha.insert("_43690")
        banco.insert(sistema_)
        filter_ = 'False'
    elif tipo_banco == 'Oracle':
        a,b,c,d,e = 1,1,1,0,0
        filter_ = 'False'
    
    servidor.setEnabled(a)
    usuario.setEnabled(b)
    senha.setEnabled(c)
    banco.setEnabled(d)
    procura_banco.setEnabled(e)

def procuraBanco(banco, QFileDialog):
    
    global filter_
    
    if filter_ == 'False':
        return False
    else:
        banco.clear()
        banco.insert(QFileDialog.getOpenFileName(None, 'Abrir banco', filter=filter_)[0])

def digitaDado(status, dado, tipo, sistema):
    
    dado.clear()
    
    if bool(status):
        dado.setReadOnly(0)
        tipo.setEnabled(1)
    else:
        dado.setReadOnly(1)
        tipo.setEnabled(0)
        tipo.setCurrentIndex(0)
        
    if sistema != "Ponto Secullum 4":
        tipo.setCurrentIndex(0)
        tipo.setEnabled(0)
    else:
        tipo.setCurrentIndex(0)
        tipo.setEnabled(1)
        
def alteraDado(tipo, dado):
    
    dado.clear()
    
    if tipo == 'CNPJ':
        dado.setInputMask("00.000.000/0000-00")
    else:
        dado.setInputMask("000.000.000-00")
    
def adicionaRevendas(cur, revenda):
    revenda.clear()
    revenda.addItem("")
    cur.execute("select distinct(revenda) from acessos_zerados")
    lista = [x[0] for x in cur.fetchall()]
    revenda.addItems(lista)
    
def limpaCampos(razao, tipo_dado, dado, status, revenda, sistema, motivo, tipo_banco, servidor, usuario, senha, banco, procura_banco):
    razao.clear()
    razao.insert("(Automatico)")
    tipo_dado.setCurrentIndex(0)
    tipo_dado.setEnabled(0)
    dado.clear()
    status.setChecked(0)
    revenda.setCurrentIndex(0)
    revenda.setFocus()
    sistema.setCurrentIndex(0)
    motivo.clear()
    tipo_banco.setCurrentIndex(0)
    servidor.clear()
    usuario.clear()
    senha.clear()
    banco.clear()
    banco.insert("psec.dat")
    procura_banco.setEnabled(1)
    