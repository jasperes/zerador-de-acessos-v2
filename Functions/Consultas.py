# -*- coding: utf-8 -*-

"""
Ações dos widgets da tela de Consultas
"""

from PySide.QtGui import QTableWidgetItem

def inseri(cursor, cliente, revenda, dado):
    cursor.execute("select cliente, revenda, dado from acessos_zerados")
    
    a = list()
    b = list()
    c = list()
    
    for x in cursor.fetchall():
        a.append(x[0])
        b.append(x[1])
        c.append(x[2])
        
    cliente.addItem("")
    revenda.addItem("")
    dado.addItem("")
    
    cliente.addItems(list(set(a)))
    revenda.addItems(list(set(b)))
    dado.addItems(list(set(c)))
    
def reseta(cliente, revenda, dado, sistema, table):
    cliente.setCurrentIndex(0)
    revenda.setCurrentIndex(0)
    dado.setCurrentIndex(0)
    sistema.setCurrentIndex(0)
    table.setRowCount(0)
    
    
def pesquisa(cursor, cliente, revenda, dado, sistema, table):
    if sistema == "Todos":
        sistema = ""
        
    query = "select cliente, revenda, dado, data, computador, motivo, sistema from acessos_zerados"
    query += " where cliente like '%"+cliente+"%'"
    query += " and revenda like '%"+revenda+"%'"
    query += " and dado like '%"+dado+"%'"
    query += " and sistema like '%"+sistema+"%'"

    cursor.execute(query)

    rows = list()
    for x in cursor.fetchall():
        rows.append(x)
    
    table.clear()
    table.setColumnCount(6)
    table.setHorizontalHeaderLabels(('Cliente', 'Revenda', 'Dado', 'Data', 'Computador', 'Motivo', 'sistema'))
    table.setRowCount(len(rows))
    
    for row, cols in enumerate(rows):
        for col, text in enumerate(cols):
            table_item = QTableWidgetItem(str(text))
            # Optional, but very useful.
            #table_item.setData(Qt.UserRole+1, user)
            table.setItem(row, col, table_item)
        
    table.resizeColumnsToContents()