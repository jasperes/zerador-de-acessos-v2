# -*- coding: utf-8 -*-
# Python3

"""
Zera os acessos do banco de dados.
Inicia exibindo um QProgressBar.

Realiza as seguintes operações:
    - Verifica se os campos estão preenchidos corretamente
    - Verifica se a conexão com o banco de dados está ok
    - Verifica se a empresa já foi zerada
    - Zera os acessos do banco
    - Gera um backup
    - Fecha a conexção com o banco
    - Compacta o backup no local C:\Backup_Zerado\<nome_do_arquivo>
    - Grava o "zeramento" no banco de dados da aplicação

Por fim, abre o EXPLORER exibindo o arquivo compactado
"""

from os import mkdir, remove, system
from os.path import exists
from datetime import date
from zipfile import ZipFile
from shutil import copyfile
from socket import gethostname

import pyodbc
import pymssqlce

from PySide.QtGui import QProgressDialog, QMessageBox
from PySide.QtCore import Qt

from Functions.Funcoes import adicionaRevendas #limpaCampos

class Start(QProgressDialog):

    def __init__(self,db,connector,configs,razao,tipo_dado,dado,manual,revenda,sistema,motivo,tipo_banco,servidor,usuario,senha,nome_banco,procura_banco,parent=None):
        
        QProgressDialog.__init__(self,parent)
        
        self.db = db
        self.connector = connector
        self.configs = configs
        self.drivers = configs['DRIVERS']
        self.backup_path = configs['BACKUP PATH']['path']
        self.backup_path_ = configs['BACKUP PATH']['path_']
        self.compressao = configs['BACKUP PATH']['compress_mode']
        self.tipo_dado = str(tipo_dado.currentText())
        self.dado = str(dado.text())
        self.manual = bool(manual.checkState())
        self.revenda = str(revenda.currentText())
        self.sistema = str(sistema.currentText())
        self.motivo = str(motivo.toPlainText())
        self.tipo_banco = str(tipo_banco.currentText())
        self.servidor = str(servidor.text())
        self.usuario = str(usuario.text())
        self.senha = str(senha.text())
        self.nome_banco = str(nome_banco.text())

        self.setWindowModality(Qt.WindowModal)
        self.setMinimum(0)
        self.setMaximum(8)
        self.setWindowTitle("Processando...")
        self.setCancelButton(None)
        
        self.setLabelText("Verificando campos")
        
        self.executa(dado, revenda, razao)
        
    def executa(self, dado, revenda, razao):
        
        if self.verificaCampos() == 1:
            self.setLabelText("Verificando banco de dados")
            self.setValue(1)
        else:
            return False
        
        if self.verificaBanco() == 1:
            self.setLabelText("Verificando Empresa")
            self.setValue(2)
        else:
            return False
                
        if self.verificaEmpresa() == 1:
            self.setLabelText("Zerando Acessos")
            self.setValue(3)
        else:
            return False
        
        if self.zeraAcessos() == 1:
            self.setLabelText("Gerando Backup")
            self.setValue(4)
        else:
            return False
        
        if self.geraBackup() == 1:
            self.setLabelText("Fechando conexão")
            self.setValue(5)
        else:
            return False
                            
        if self.fechaConexao() == 1:
            self.setLabelText("Compactando Backup")
            self.setValue(6)
        else:
            return False
                            
        if self.compactaBackup() == 1:
            self.setLabelText("Gravando Ação")
            self.setValue(7)
        else:
            return False
                                    
        if self.gravaAcao() == 1:
            self.setLabelText("Finalizando")
            self.setValue(8)
            msg = "O banco de dados foi zerado e compactado. Você pode encontralo no seguinte diretório:"
            msg += "\n"+self.backup_path+self.backup
            msg += "Senha: %s" % self.senha_compressada
            if self.log != "": 
                msg += "\n\n*** LOG ***\n"+self.log
            QMessageBox.information(self, "Processo executado com exito!", msg)
            system("explorer /select, \""+self.backup_path+"\"" + self.revenda + "-" + self.data + ".zip\"")
            adicionaRevendas(self.db, revenda)
            dado.insert(self.dado)
            index = revenda.findText(self.revenda)
            revenda.setCurrentIndex(index)
            razao.clear()
            razao.insert(str(self.razao))
        else:
            return False
        
        self.deleteLater()

    def verificaCampos(self):
        
        if self.manual:
            if self.tipo_dado == 'CNPJ':
                if len(self.dado.replace(" ","")) < 18:
                    return QMessageBox.warning(self, "Atenção!", "Preencha o campo CNPJ corretamente!")
            else:
                if len(self.dado.replace(" ","")) < 14:
                    return QMessageBox.warning(self, "Atenção!", "Preencha o campo CPF corretamente!")
                
        if self.revenda.replace(" ","") == "":
            return QMessageBox.warning(self, "Atenção!", "Preencha o campo Revenda corretamente!")
            
        if ((self.motivo.replace(" ","")).replace("\t","")).replace("\n","") == "":
            return QMessageBox.warning(self, "Atenção!", r"Preencha o campo Motivo corretamente!")
            
        if self.tipo_banco == "Access" or self.tipo_banco == "SQL Server CE":
            if self.nome_banco.replace(" ","") == "":
                return QMessageBox.warning(self, "Atenção!", "Prenecha o caminho do banco corretamente!")
        elif self.tipo_banco == "SQL Server" or self.tipo_banco == "Firebird":
            if self.servidor.replace(" ","") == "" or self.usuario.replace(" ","") == "" or self.senha.replace(" ","") == "" or self.nome_banco == "":
                return QMessageBox.warning(self, "Atenção!", "Preencha os dados de conexao corretamente!")
        elif self.tipo_banco == "Oracle":
            if self.servidor.replace(" ","") == "" or self.usuario.replace(" ","") == "" or self.senha.replace(" ","") == "":
                return QMessageBox.warning(self, "Atenção!", "Preencha os dados de conexao corretamente!")
                
        return True

    def verificaBanco(self):
        
        if self.tipo_banco == 'Access':
            DSN = 'DRIVER={%s};DBQ=%s' % (self.drivers['access'],self.nome_banco)
        elif self.tipo_banco == 'SQL Server':
            DSN = 'DRIVER={%s};SERVER=%s;DATABASE=%s;UID=%s;PWD=%s;' % (self.drivers['sql server'],self.servidor, self.nome_banco, self.usuario, self.senha)
        elif self.tipo_banco == 'Oracle':
            DSN = 'DRIVER={%s};UID=%s;PWD=%s;SERVER=%s;' % (self.drivers['oracle'],self.usuario, self.senha, self.servidor)
        elif self.tipo_banco == 'Firebird':
            DSN = 'Driver={%s};Uid=%s;Pwd=%s;DbName=localhost:%s;' % (self.drivers['firebird'],self.usuario, self.senha, self.nome_banco)
        else:
            DSN = ""
            
        if DSN == "":
            try:
                self.conn = pymssqlce.connect(self.nome_banco)
                self.cur = self.conn.cursor()
                return True
            except Exception as err:
                return QMessageBox.critical(self, "Erro!", "Houve um erro ao conectar com o banco de dados\n\n"+str(err))
        
        try:
            self.conn = pyodbc.connect(DSN)
            self.cur = self.conn.cursor()
            return True
        except pyodbc.OperationalError as err:
            return QMessageBox.critical(self, "Falha ao conectar com o banco de dados", str(err))
        except pyodbc.DataError as err:
            return QMessageBox.critical(self, "Falha ao conectar com o banco de dados", str(err))
        except pyodbc.IntegrityError as err:
            return QMessageBox.critical(self, "Falha ao conectar com o banco de dados", str(err))
        except pyodbc.ProgrammingError as err:
            return QMessageBox.critical(self, "Falha ao conectar com o banco de dados", str(err))
        except pyodbc.NotSupportedError as err:
            return QMessageBox.critical(self, "Falha ao conectar com o banco de dados", str(err))
        except pyodbc.DatabaseError as err:
            return QMessageBox.critical(self, "Falha ao conectar com o banco de dados", str(err))
        except pyodbc.Error as err:
            return QMessageBox.critical(self, "Falha ao conectar com o banco de dados", str(err))
        except Exception as err:
            return QMessageBox.critical(self, "Falha ao conectar com o banco de dados", str(err))

        return None

    def verificaEmpresa(self):
        
        n = 2 if self.sistema == "Ponto Secullum 4" else 1
            
        try:
            if self.tipo_banco == "Access":
                self.cur.execute("select top %i nome, cnpj from empresas" % n)
            elif self.tipo_banco == "Firebird":
                self.cur.execute("select first %i nome, cnpj from empresas" % n)
            elif self.tipo_banco == "Oracle":
                self.cur.execute("select nome, cnpj from empresas where rownum <= %i" % n)
            else:
                self.cur.execute("select top(%i) nome, cnpj from empresas" % n)
        except Exception as err:
            return QMessageBox.critical(self, "Erro!", "Erro ao selecionar dados da empresa\n\n"+str(err))
            
        self.razao = ""
        cnpj = ""
        count = 0
        
        for x in self.cur.fetchall():
            self.razao = x[0]
            cnpj = x[1]
            count += 1
            
        if count == 0:
            return QMessageBox.warning(self, "Aviso!", "Nenhuma empresa cadastrada neste banco de dados")
        
        if cnpj.replace(" ","") == "00.000.000/0000-00":
            return QMessageBox.warning(self, "Aviso!", "Apenas empresa Relógio Teste encontrado no banco de dados.")
            
        if cnpj.replace(" ","") == "":
            return QMessageBox.warning(self, "Aviso!", "Banco de dados sem CNPJ. Por favor, preencha manualmente ou contate o supervisor.")
            
        if not self.manual:
            self.dado = cnpj

        try:
            self.db.execute("select id from acessos_zerados where dado='%s'" % self.dado)
            n_zerado = len(self.db.fetchall())
        except Exception as err:
            return QMessageBox.critical(self, "Erro!", "Erro ao selecionar dados de acessos zerados\n\n"+str(err))
        
        if n_zerado > 0:
            if (QMessageBox.question(self, "Aviso!", u"Este banco de dados já foi zerado "+str(n_zerado)+" vezes. Deseja continuar?\n\n"+
                "IMPORTANTE: Este procedimento devera ser cobrado. Qualquer dúvida, consulte o supervisor.",
                QMessageBox.Yes | QMessageBox.No)) == QMessageBox.No:
                return False

        return True
        
    def zeraAcessos(self):
        
        try:
            self.cur.execute("update registro_sistema_config set acessos=null")
            self.conn.commit()
            return True
        except Exception as err:
            return QMessageBox.warning(self, "Erro ao registrar zeração!", str(err))
            
        return None
    
    def geraBackup(self):
        
        if not exists(self.backup_path_):
            mkdir(self.backup_path_)
            
        self.data = (date.today()).strftime("%d-%m-%Y")
        
        def verificaBackup(backup):
            
            if exists(self.backup_path+backup):
                if (QMessageBox.question(self, "Aviso!", "ja esiste um backup com o mesmo nome. Deseja excluí-lo?\n\n"+
                    "IMPORTANTE: Se você cancelar agora, devera executar este novamente.",
                    QMessageBox.Yes | QMessageBox.No)) == QMessageBox.Yes:
                        remove(self.backup_path+backup)
                else:
                    return QMessageBox.warning(self, "Cancelado!", "Processo cancelado com êxito")
        
        if self.tipo_banco == "Access":
            
            self.backup = "psec.dat"
            verificaBackup(self.backup)
            copyfile(self.nome_banco, self.backup_path+self.backup)
            return True
        
        elif self.tipo_banco == "SQL Server CE":
            
            self.backup = (str(self.sistema).replace(" ","")).replace(".","")+".sdf"
            verificaBackup(self.backup)
            copyfile(self.nome_banco, self.backup_path+self.backup)
            return True
        
        elif self.tipo_banco == "Firebird":
            
            self.backup = (str(self.sistema).replace(" ","")).replace(".","")+".fdb"
            verificaBackup(self.backup)
            copyfile(self.nome_banco, self.backup_path+self.backup)
            return True
        
        elif self.tipo_banco == "SQL Server":
            
            self.backup = str(self.razao).replace("/", " ").replace("\\", " ") + "-" + self.data + ".bak"
            verificaBackup(self.backup)
            query = "BACKUP DATABASE %s TO DISK = '%s'" % (self.nome_banco, self.backup_path+self.backup)
                
            if self.configs['BACKUP SQL SERVER']['mode'] == 'cmd':
                cmd = "\""+self.configs['BACKUP SQL SERVER']['cmd_path']+"\""
                cmd += ' -U '+self.usuario
                cmd += ' -P '+self.senha
                cmd += ' -S '+self.servidor
                cmd += ' -Q \"'+query+"\""
                
                try:
                    if system(cmd):
                        raise Exception()
                    return True
                except Exception as err:
                    return QMessageBox.critical(self, 'Erro!', str(err))
                
            try:
                self.cur.execute(query)
                while self.cur.nextset():
                    pass
                return True
            except pyodbc.OperationalError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.DataError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.IntegrityError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.ProgrammingError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.NotSupportedError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.DatabaseError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.Error as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except Exception as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            
        elif self.tipo_banco == 'Oracle':
            
            try:
                self.backup = str(self.razao).replace("/", " ").replace("\\", " ") + "-" + self.data +".dmp"
                verificaBackup(self.backup)
                self.cur.execute("CREATE OR REPLACE DIRECTORY backup_zerado AS '%s';" % self.backup_path_)
                self.cur.execute("GRANT READ, WRITE ON DIRECTORY backup_zerado TO %s;" % self.usuario)
                self.cur.execute("expdp %s full=Y directory=backup_zerado dumpfile=%s.dmp logfile=%s.log" 
                % (self.usuario+'/'+self.senha+'@'+self.servidor,self.backup,self.backup))
                return True
            except pyodbc.OperationalError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.DataError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.IntegrityError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.ProgrammingError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.NotSupportedError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.DatabaseError as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except pyodbc.Error as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))
            except Exception as err:
                return QMessageBox.critical(self, "Falha ao gerar backup", str(err))

        return None
        
    def fechaConexao(self):
        
        try:
            self.conn.close()
            self.log = ""
        except Exception as err:
            self.log = "Nao foi possível fechar conexao com o banco de dados. Feche este aplicativo para fechar a conexao"
            self.log += "\n\n"+str(err)

        return True
        
    def compactaBackup(self):

        zipar = self.backup_path+self.revenda + "-" + self.data + ".zip"
        self.senha_compressada = self.revenda.lower().replace(" ","")+str(date.today().year)
        sevenZ = ('7-Zip\\7z.exe a -p%s "%s" "%s"' % (self.senha_compressada, zipar, self.backup_path+self.backup))
        
        if exists(zipar):
            try:
                remove(zipar)
            except Exception as err:
                return QMessageBox.critical(self, "Falha ao remover arquivo ja compactado", str(err))
        
        if self.compressao == "senha":
            if system(sevenZ) == 1:
                return QMessageBox.critical(self, "Erro!", "Houve um erro durante a compactação.\nVocê pode tentar o modo seguro.")
        elif self.compressao == "seguro":
            try:
                with ZipFile(zipar, "w") as zipa:
                    zipa.write(self.backup_path+self.backup, self.backup)
            except Exception as err:
                return QMessageBox.critical(self, 'Erro!', 'Erro ao compactar\n\n'+(str(err)))
        else:
            return QMessageBox.critical(self, "Erro!", "Falha ao verificar parâmetro de compactação!")

        try:
            remove((self.backup_path+self.backup))
        except Exception as err:
            self.log += "\nNão foi possível apagar o backup. Por favor, apague-o manualmente.\nException: "+str(err)
        
        return True
        
    def gravaAcao(self):
        
        query = "insert into acessos_zerados(cliente, dado, revenda, sistema, data, motivo, computador) "
        query += "values("
        query += "'"+self.razao+"',"
        query += "'"+self.dado+"',"
        query += "'"+self.revenda+"',"
        query += "'"+self.sistema+"',"
        query += "'"+(date.today()).strftime("%d/%m/%Y")+"',"
        query += "'"+self.motivo+"',"
        query += "'"+str(gethostname())+"')"
        
        try:
            self.db.execute(query)
            self.connector.commit()
        except pyodbc.OperationalError as err:
            return QMessageBox.critical(self, "Falha ao gravar ação", str(err)+'\n1\n'+query)
        except pyodbc.DataError as err:
            return QMessageBox.critical(self, "Falha ao gravar ação", str(err)+'\n2\n'+query)
        except pyodbc.IntegrityError as err:
            return QMessageBox.critical(self, "Falha ao gravar ação", str(err)+'\n3\n'+query)
        except pyodbc.ProgrammingError as err:
            return QMessageBox.critical(self, "Falha ao gravar ação", str(err)+'\n4\n'+query)
        except pyodbc.NotSupportedError as err:
            return QMessageBox.critical(self, "Falha ao gravar ação", str(err)+'\n5\n'+query)
        except pyodbc.DatabaseError as err:
            return QMessageBox.critical(self, "Falha ao gravar ação", str(err)+'\n6\n'+query)
        except pyodbc.Error as err:
            return QMessageBox.critical(self, "Falha ao gravar ação", str(err)+'\n7\n'+query)
        except Exception as err:
            return QMessageBox.critical(self, "Falha ao gravar ação", str(err)+'\n8\n'+query)

        return True




